<?php

declare(strict_types=1);

namespace JsemAjtacka\TextPage\DI;

use Nette\DI\CompilerExtension;

final class TextPageExtension extends CompilerExtension {}