<?php

declare(strict_types=1);

namespace JsemAjtacka\TextPage;

use Knp\DoctrineBehaviors\Contract\Entity\TranslationInterface;

interface TextPageTranslation extends TranslationInterface
{
    public function getTitle(): string;
    public function getContent(): string;
}
