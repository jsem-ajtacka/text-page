<?php

declare(strict_types=1);

namespace JsemAjtacka\TextPage;

use Knp\DoctrineBehaviors\Contract\Entity\TranslatableInterface;

interface TextPage extends TranslatableInterface
{
    public function getId(): int;
}
