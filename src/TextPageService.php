<?php

declare(strict_types=1);

namespace JsemAjtacka\TextPage;

interface TextPageService
{
    public function getItemBySlug(string $slug) : TextPage|null;
}