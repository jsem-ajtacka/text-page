<?php

declare(strict_types=1);

namespace JsemAjtacka\TextPage\Presenters;

use Contributte\Translation\Translator;
use JsemAjtacka\TextPage\TextPageService;
use Nette\Application\UI\Presenter;

class TextPagePresenter extends Presenter
{
    public function __construct(private TextPageService $textPageService,
                                private Translator $translator) {
        parent::__construct();
    }

    public function renderDefault($slug): void
    {
        $item = $this->textPageService->getItemBySlug($slug);

        $this->template->page = $item->translate($this->translator->getLocale());
    }
}